package pl.edu.pjwstk.mpr.main;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import pl.edu.pjwstk.mpr.db.DBManager;
import pl.edu.pjwstk.mpr.main.Denat;
import pl.edu.pjwstk.mpr.main.MenuAction;
import pl.edu.pjwstk.mpr.main.TxtInterface;
import pl.edu.pjwstk.mpr.main.UserInterface;
/**
 * 
 * @author Bartosz Zagórski
 *
 */
public class Start {

	private String dbName = "zakladpogrzebowy";
	private UserInterface ui = new TxtInterface();
	private DBManager dbManager = new DBManager();
	
	public static void main(String[] args) throws SQLException{
		
		new Start().go();
		
	}

	/**
	 * Przygotowania i główna pętla programu.
	 * @throws SQLException 
	 */
	private void go() throws SQLException {

		ui.showGreeting();
		
		try {
			dbManager.connectToDb(dbName);
		} catch (SQLException e) {
			fatalError("Nie udalo sie polaczyc do bazy " + dbName +
					"\n(" + e.getMessage() +")" +
					"\n(sprawdź, czy serwer jest na pewno uruchomiony)");
			return;
		}
		
		while ( doOneAction() ) {}

	}
	
	/**
	 * Pojedyncze wyświetlenie menu i wykonanie akcji wybranej przez usera.
	 * @return true jesli user chce zakonczyc prace z programem
	 * @throws SQLException 
	 */

	private boolean doOneAction() throws SQLException {
		
		MenuAction action;
		do {
			action = ui.showMenuAndGetAction();
		} while (action == MenuAction.INVALID);
		
		switch (action) {
		case SHOW_ALL :
			listDenat();
			break;
		case SHOW_CM :
			listCmentarz();
			break;
		case ADD :
			Denat m = ui.getDenatData();
			try {
				dbManager.saveDenatInfo(m);
				ui.showMsg("Udalo sie zapisac zmarlego "+m);
			} catch (SQLException e) {
				ui.showErrorMsg("Nie udalo sie zapisac zmarlego "+m);
				e.printStackTrace();
			}
			break;
		case ADD_CM :
			Cmentarz cm = ui.getCmentarzData();
			try {
				dbManager.saveCmentarzInfo(cm);
				ui.showMsg("Udalo sie zapisac cmentarz "+cm);
			} catch (SQLException e) {
				ui.showErrorMsg("Nie udalo sie zapisac cmentarza "+cm);
				e.printStackTrace();
			}
			break;
		case DELETE :
			if (!listDenat()) break;
			int id;
			while ((id = ui.getDenatId()) < 0)
				ui.showErrorMsg("Podano niepoprawne id\n(wiadomo co ma być).");
			if (dbManager.deleteDenat(id) == true)
				break;
			else
				ui.showErrorMsg("Nie ma denatu o takim id.");
			break;
		case QUIT :
			ui.showMsg("Zakończono pracę programu.");
			return false;
		}
		
		return true;
	}
	

	/**
	 * Wyświetla listę denatow wczytaną z bazy.
	 * @return true jeśli byly jakieś rekordy
	 */


	private boolean listDenat() {
		try {
			List<Denat> denat = dbManager.getAllDenat();
			if (denat==null || denat.isEmpty()) {
				ui.showMsg("W bazie nie ma żadnych denatow");
				return false;
			}
			ui.listDenat(denat);
		} catch (SQLException e) {
			fatalError("Nie udało się pobrać danych rekordow z bazy" +
					"\n("+e.getMessage()+")");
		}
		return true;
	}
	
	private boolean listCmentarz() {
		try {
			List<Cmentarz> cmentarz = dbManager.getAllCmentarz();
			if (cmentarz==null || cmentarz.isEmpty()) {
				ui.showMsg("W bazie nie ma żadnych cmentarzy");
				return false;
			}
			ui.listCmentarz(cmentarz);
		} catch (SQLException e) {
			fatalError("Nie udało się pobrać rekordow z bazy" +
					"\n("+e.getMessage()+")");
		}
		return true;
	}
	
	/**
	 * Reakcja na błąd uniemożliwiający dalszą pracę aplikacji
	 * @param msg wiadomość do zaprezentowania userowi
	 */
	private void fatalError(String msg) {
		ui.showErrorMsg(msg);
		ui.showErrorMsg("Ten błąd uniemożliwia dalszą pracę programu. Kończę.");
		System.exit(-1);
	}

}
