package pl.edu.pjwstk.mpr.main;
import pl.edu.pjwstk.mpr.main.*;

public class Denat {
	private int id = -1;
	private String imie;
	private String nazwisko;
	private String pesel;
	private String wiek;
	private int cmentarz_id;
	/**
	 * 
	 * @param imie imię klienta
	 * @param nazwisko nazwisko klienta
	 * @param pesel pesel klienta
	 * @param wiek wiek klienta
	 */
	public Denat(String imie, String nazwisko, String pesel, String wiek, int cmentarz_id){
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.pesel = pesel;
		this.wiek = wiek;
		this.cmentarz_id=cmentarz_id;
		
	}
	
	public Denat(int id, String imie, String nazwisko, String pesel, String wiek, int cmentarz_id){
		this(imie, nazwisko, pesel, wiek, cmentarz_id);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImie() {
		return imie;
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}
	
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getPesel() {
		return pesel;
	}
	
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public String getWiek() {
		return wiek;
	}
	
	public void setWiek(String wiek) {
		this.wiek = wiek;
	}
	
	public int getCmentarz_id(){
		return cmentarz_id;
	}

	public void setCmentarz_id(int cmentarz_id) {
		this.cmentarz_id = cmentarz_id;
	}
	
	public String toString() {
		return ((id==-1)? "": id+") ") +" imię: "+imie+" nazwisko: "+nazwisko+
			" pesel: "+pesel+" wiek: "+wiek+  " Nr cmentarza: " + cmentarz_id;
	}
	
}
