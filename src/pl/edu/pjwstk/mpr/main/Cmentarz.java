package pl.edu.pjwstk.mpr.main;
import pl.edu.pjwstk.mpr.main.*;

public class Cmentarz {
	private int id = -1;
	private String nazwa;
	
	public Cmentarz(String nazwa){
		this.nazwa = nazwa;
	}
	
	public Cmentarz(int id, String nazwa){
		this(nazwa);
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazwa(){
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
	public String toString() {
		return ((id==-1)? "": id+") ") +nazwa;
	}
}
