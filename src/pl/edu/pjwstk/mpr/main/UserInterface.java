package pl.edu.pjwstk.mpr.main;

import java.util.List;

import pl.edu.pjwstk.mpr.main.Denat;

public interface UserInterface {

	/**
	 * Wyswietla powitanie usera.
	 */
	void showGreeting();
	
	/**
	 * Wyświetla menu aplikacji i zwraca pozycję wybraną przez usera.
	 * @return
	 */
	MenuAction showMenuAndGetAction();

	/**
	 * Prezentuje userowi komunikat.
	 * @param string
	 */
	void showMsg(String msg);
	
	/**
	 * Prezentuje userowi komunikat błędu.
	 * @param string
	 */
	void showErrorMsg(String msg);

	/**
	 * Wyświetla dane zmarlych.
	 * @param denat
	 */
	void listDenat(List<Denat> denat);
	
	void listCmentarz(List<Cmentarz> cmentarz);
	
	/**
	 * Pobiera od użytkownika informacje o zmarłym.
	 * @return
	 */
	Denat getDenatData();

	Cmentarz getCmentarzData();
	
	/**
	 * Pobiera id zmarlego.
	 * @return
	 */
	int getDenatId();
	
	
}
