package pl.edu.pjwstk.mpr.main;

public enum MenuAction {
	SHOW_ALL,
	SHOW_CM,
	ADD,
	ADD_CM,
	DELETE,
	QUIT,
	INVALID ;
	public static MenuAction parse(String str) {
		try {
			int ret = Integer.valueOf(str);
			switch(ret) {
				case 1: return SHOW_ALL;
				case 2: return ADD;
				case 3: return DELETE;
				case 4: return SHOW_CM;
				case 5: return ADD_CM;
				case 6: return QUIT;
				default: return INVALID;
			}
		} catch (NumberFormatException e) {
			return INVALID;
		}
	}
}
