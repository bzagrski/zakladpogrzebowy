package pl.edu.pjwstk.mpr.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

import pl.edu.pjwstk.mpr.main.Denat;

public class TxtInterface implements UserInterface {

	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void showGreeting() {
		System.out.println("+----------------------------------------+\n"+
				           "|     Zakład Pogrzebowy Radość życia     |\n"+
				           "+----------------------------------------+");
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public MenuAction showMenuAndGetAction() {
		System.out.println("\nCo checsz zrobić?\n" +
				"1. Wypisać dane zmarłych z bazy.\n" +
				"2. Dodać nowego zmarłego.\n" +
				"3. Usunąć zmarłego.\n" +
				"4. Wypisać cmentarze.\n"+
				"5. Dodać cmentarz\n"+
				"6. Zakończyć program\n" +
				"(wpisz cyfrę od 1 do 6)");
		String choice = getUserInput();
		return MenuAction.parse(choice);
	}

	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void listDenat(List<Denat> denat) {
		for (Denat d: denat) {
			System.out.println(d);
		}
	}
	
	@Override
	public void listCmentarz(List<Cmentarz> cmentarz) {
		for (Cmentarz cm: cmentarz) {
			System.out.println(cm);
		}
	}
	
	@Override
	public Denat getDenatData() {
		Scanner input = new Scanner(System.in);
		System.out.println("Podaj dane zmarlego");
		System.out.println("imie : ");
		String imie = getUserInput();
		System.out.println("nazwisko : ");
		String nazwisko = getUserInput();
		System.out.println("pesel : ");
		String pesel = getUserInput();
		System.out.println("wiek : ");
		String wiek = getUserInput();
		System.out.println("ID cmentarza:");
		int cmentarz_id = input.nextInt();
		return new Denat(imie, nazwisko, pesel, wiek, cmentarz_id);
	}
	
	@Override
	public Cmentarz getCmentarzData() {
		Scanner input = new Scanner(System.in);
		System.out.println("Podaj dane cmentarza");
		System.out.println("nazwa : ");
		String nazwa = getUserInput();
		return new Cmentarz(nazwa);
	}
	
	@Override
	public int getDenatId() {
		System.out.println("Podaj id zmarlego : ");
		try {
			return Integer.parseInt(getUserInput());
		} catch (NumberFormatException e) {
			return -1;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void showMsg(String msg) {
		System.out.println(msg);
	}
	
	/**
	 * {@inheritDoc}
	 */	
	@Override
	public void showErrorMsg(String msg) {
		System.err.println("[BŁĄD] "+msg+"\n");
	}
	
	/**
	 * Pobiera i zwraca odpowiedź usera.
	 * @return
	 */
	private String getUserInput() {
		try {
			return br.readLine();
		} catch (IOException e) {
			System.err.println("Problem z wczytywaniem odpowiedzi użytkownika");
			System.exit(-1);
			return null;
		}
	}
	
}
